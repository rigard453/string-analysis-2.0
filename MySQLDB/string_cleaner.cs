﻿using System;
using MySql.Data.MySqlClient;

namespace MySQLDB;
public class WorkSQLDB
{
    /// <summary>
    /// класс по работе с базой данных
    /// </summary>
    /// 
    //данные для подключения
    private string nameDB = "Rokfellers";
    private string server = "localhost";
    private string login = "rigard";
    private string password = "password";
    private string port = "3306";
    private string connectionString = "";
    //private MySQLDB connInfo;
    private MySqlConnection conn;
    private MySqlCommand cmd;

    //инициализирующий класс
    public void MySQLDB( )
    {
        connectionString = "Server=" + server + ";Database=" + nameDB + ";port=" + port + ";User Id=" + login + ";password=" + password;
    }
    //сеттеры

    public void set_nameDB(string NameDB)
    {
        nameDB = NameDB;
        set_connectionString();
    }
    /// <summary>
    /// метод создания соединения с БД
    /// </summary>
    public bool DBConnect( )
    {
        //создание нового подключения
        // MySQLDB connInfo = new MySQLDB();
        // cоединение с БД
        conn = new MySqlConnection(get_connectionString());
        //подкотавливаем к командам
        cmd = conn.CreateCommand();

        try
        {
            Console.WriteLine("Openning Connection ...");
            // устанавливаем соединение с БД
            conn.Open();


            Console.WriteLine("Connection successful!");
            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
            return false;
        }
    }

    /// <summary>
    /// возвращает объект соединение
    /// </summary>
    /// <returns></returns>
    public MySqlConnection GetMySqlConnection( )
    {
        return conn;
    }

    /// <summary>
    /// возвращает объект с командами
    /// </summary>
    /// <returns></returns>
    public MySqlCommand GetMySqlCommand( )
    {
        return cmd;
    }

    public void set_server(string Server)
    {
        server = Server;
        set_connectionString();
    }

    public void set_login(string Login)
    {
        login = Login;
        set_connectionString();
    }

    public void set_password(string Password)
    {
        password = Password;
        set_connectionString();
    }

    public void set_port(string Port)
    {
        port = Port;
        set_connectionString();
    }

    /// <summary>
    /// метод выводит данные о подсоединении
    /// </summary>
    public void printConectionData( )
    {
        Console.WriteLine($"");
        Console.WriteLine($"Данные для подключения:");
        Console.WriteLine($"1. База данных : {nameDB}.");
        Console.WriteLine($"2. Сервер : {server}.");
        Console.WriteLine($"3. Порт : {port}.");
        Console.WriteLine($"4. Пользователь : {login}.");
        Console.WriteLine($"5. Пароль : {password}.");
        Console.WriteLine($"6. Строка подключения : {connectionString}.");

        Console.WriteLine($"");

    }

    /// <summary>
    /// метод генерирует строку данных для подключения к БД
    /// </summary>
    private void set_connectionString( )
    {
        string connectionString = "Server=" + server + ";Database=" + nameDB + ";port=" + port + ";User Id=" + login + ";password=" + password;
    }

    /// <summary>
    /// метод возвращает строку данных для подключения к БД
    /// </summary>
    /// <returns></returns>
    public string get_connectionString( )
    {
        return connectionString;
    }

    /// <summary>
    /// Метод записи в базу данных
    /// </summary>
    /// <param name="mytext"> строка с данными</param>

    public void inSertText(string mytext)
    {
        try
        {

            string myText = mytext;
            string sql = "insert page_text (text) " + " values (@myText) ";

            //запишем в базу даных
            cmd.CommandText = sql;

            //наполняет команду для изменения базы данных
            //cmd.Parameters.AddWithValue("@CarId", id);
            cmd.Parameters.AddWithValue("@myText", myText);
            // Выполнить Command (использованная для  delete, insert, update).
            //непосредственно вносит данные в базу даных сформированной командой
            cmd.ExecuteNonQuery();
            //очистка параметров для дальнейшей работы
            cmd.Parameters.Clear();

        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
            try
            {
                cmd.Parameters.Clear();
                string sql = "insert page_text (text) " + " values (@myText) ";
                //запишем в базу даных
                cmd.CommandText = sql;
                //наполняет команду для изменения базы данных
                //cmd.Parameters.AddWithValue("@CarId", id);
                cmd.Parameters.AddWithValue("@myText", "Слишком большие данные и не поместились в поле базы данных.");
                // Выполнить Command (использованная для  delete, insert, update).
                //непосредственно вносит данные в базу даных сформированной командой
                cmd.ExecuteNonQuery();
                //очистка параметров для дальнейшей работы
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }


        }
        finally
        {
            cmd.Parameters.Clear(); ;
        }
    }

    /// <summary>
    /// Метод записи в базу данных URL
    /// </summary>
    public void inSertURL(string URL)
    {
        try
        {

            string myText = URL;
            string sql = "insert URLTable (url) " + " values (@myText) ";

            //запишем в базу даных
            cmd.CommandText = sql;

            //наполняет команду для изменения базы данных
            //cmd.Parameters.AddWithValue("@CarId", id);
            cmd.Parameters.AddWithValue("@myText", myText);
            // Выполнить Command (использованная для  delete, insert, update).
            //непосредственно вносит данные в базу даных сформированной командой
            cmd.ExecuteNonQuery();
            //очистка параметров для дальнейшей работы
            cmd.Parameters.Clear();
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        finally
        {
            cmd.Parameters.Clear(); ;
        }
    }

    /// <summary>
    /// Метод записи в базу данных слов, выделенных в предложении
    /// Слова из одного предложения передаются в массиве
    /// и храняться по одним ID
    /// ID задается самостоятельно
    /// </summary>
    public void inSertWord(string[] words)
    {
        try
        {
            int idIndex;
            //команда на внесение данных
            //string sql = "insert wordInSentence (id,word) " + " values (@idIndex, @myText)";
            string sql = "insert wordInSentence (id, word) " + " values (@idIndex, @myText) ";

            //получаем максимальное значение ID из поля ID таблицы wordInSentence
            string sqlMax = "SELECT MAX(id) FROM wordInSentence";
            cmd.CommandText = sqlMax;
            if (cmd.ExecuteScalar().ToString() == "")
            {
                idIndex = 0;
            }
            else
            {
                //не хватило длины переменной
                idIndex = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            }

            idIndex++;
            // cmd.Parameters.Clear();

            foreach (var item in words)
            {
                string myText = item;
                if (item == "")
                {
                    continue;
                }


                //запишем в базу даных
                cmd.CommandText = sql;

                //наполняет команду для изменения базы данных
                cmd.Parameters.AddWithValue("@idIndex", idIndex);
                //cmd.Parameters.AddWithValue("@myText", myText);
                cmd.Parameters.AddWithValue("@myText", myText);
                // Выполнить Command (использованная для  delete, insert, update).
                //непосредственно вносит данные в базу даных сформированной командой
                cmd.ExecuteNonQuery();
                //очистка параметров для дальнейшей работы
                cmd.Parameters.Clear();
            }


        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        finally
        {
            cmd.Parameters.Clear();
        }
    }

    /// <summary>
    /// финализирует работу с базой данных и закрывает ее;
    /// </summary>
    public void closeDB( )
    {
        // Закрыть соединение.
        conn.Close();
        // Уничтожить объект, освободить ресурс.
        conn.Dispose();
    }

    /// <summary>
    /// функция чтения всей таблицы из базы данных
    /// </summary>
    /// <param name="nameTable"></param>
    public MySqlDataReader readTableDB(string nameTable)
    {
        try
        {
            string myText = nameTable;
            //sql = $"SELECT * FROM Questions WHERE ID_theme = '{Test.Theme_ID}'";
            //string sql = "insert URLTable (url) " + " values (@myText) ";
            string sql = $"SELECT * FROM {myText}";

            //прочитаем таблицу из базы данных

            cmd.CommandText = sql;

            MySqlDataReader reader = cmd.ExecuteReader();
            { //if (reader.HasRows)
                { //{
                  //    Console.WriteLine($"reader {"OK"}");
                  //}
                  //DataTable schemaTable = reader.GetSchemaTable();

                    //foreach (DataRow row in schemaTable.Rows)
                    //{
                    //    foreach (DataColumn column in schemaTable.Columns)
                    //    {
                    //        Console.WriteLine(String.Format("{0} = {1}",
                    //           column.ColumnName, row[column]));
                    //    }
                    //}

                    //while (reader.Read())
                    //{
                    //    //Console.WriteLine(reader.ToString());
                    //    Console.Write($"id =  {reader.GetInt32("id")}");
                    //    Console.WriteLine($"  {reader.GetString("trash_word")} ");
                    //}
                    //наполняет команду для изменения базы данных
                    //cmd.Parameters.AddWithValue("@CarId", id);
                    //cmd.Parameters.AddWithValue("@myText", myText);
                    // Выполнить Command (использованная для  delete, insert, update).
                    //непосредственно вносит данные в базу даных сформированной командой
                    //cmd.ExecuteNonQuery();
                    //очистка параметров для дальнейшей работы}
                }
            }
            cmd.Parameters.Clear();
            return reader;
            //reader.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        finally
        {
            cmd.Parameters.Clear();

        }

        return null;
    }

    /// <summary>
    /// удаляем строку с заданным ID
    /// </summary>
    /// <param name="id"></param>
    public void deleteRowInTable(string nameTable, string columnName, uint id)
    {

        try
        {

            //string myText = URL;
            string sql = $"DELETE FROM {nameTable}  WHERE {columnName} = {id} ";

            //запишем в базу даных
            cmd.CommandText = sql;
            { //
              ////параметризованный запрос
              //string sql = "DELETE FROM friends " +
              //"WHERE lastname = @LastName";
              ////открываем соединение с базой данных
              //con.Open();
              ////создаём команду
              //MySqlCommand cmd = new MySqlCommand(sql, con);
              ////создаем параметр и добавляем его в коллекцию
              //cmd.Parameters.AddWithValue("@LastName", lastname);
              ////выполняем sql запрос
              //cmd.ExecuteNonQuery();
              //
              //наполняет команду для изменения базы данных
              //cmd.Parameters.AddWithValue("@CarId", id);
              //cmd.Parameters.AddWithValue("@myText", myText);
            }
            // Выполнить Command (использованная для  delete, insert, update).
            //непосредственно вносит данные в базу даных сформированной командой
            cmd.ExecuteNonQuery();
            //очистка параметров для дальнейшей работы
            cmd.Parameters.Clear();
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        finally
        {
            cmd.Parameters.Clear(); ;
        }
    }
    /// <summary>
    /// Метод переписывает слова из основной таблицы в спомогательную
    /// </summary>
    public void WriteDataTable(MySqlDataReader reader, List<uint> longWord, string table_start, string tableToWrite)

    {
        //    struct rows()
        //   {
        //    public uint idprime;
        //public uint id;
        //public string = "";
        //}
        try
        {


            foreach (var item in longWord)
            {
                //читаем сохраненные IDPRIME
                string sqlMax = $"SELECT idprime, id, word FROM {table_start}  WHERE idprime = {item}";
                //читаем ID нужных слов и записывем их в таблицу базы данных и все
                //rowString = reader.GetString("word");
                cmd.CommandText = sqlMax;
                reader.Close();
                reader = cmd.ExecuteReader();
                cmd.Parameters.Clear();


                if (reader.Read())
                {
                    // Console.Write($"idprime =  {reader.GetInt32(0)} ");
                    Console.Write($"idprime =  {reader.GetInt32("idprime")} ");
                    uint idprime = (uint)reader.GetInt32("idprime");
                    uint id = (uint)reader.GetInt32("id");
                    string str = reader.GetString("word");
                    reader.Close();

                    //пишем в таблицу
                    string sql = $"insert {tableToWrite} (idprime, id, word) VALUES ('{idprime}','{id}','{str}')";
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();

                }

            }

        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        finally
        {
            cmd.Parameters.Clear();
        }
    }

    //
    /// <summary>
    /// метод отбора Рокфеллеров и не рокфеллеров
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="longWord"></param>
    /// <param name="table_start"></param>
    /// <param name="tableToWrite"></param>
    public void WriteRockfNonRockf(MySqlDataReader reader, List<uint> longWord, string table_start, string tableToWrite)

    {
        try
        {


            foreach (var item in longWord)
            {
                //читаем сохраненные IDPRIME
                string sqlMax = $"SELECT idprime, id, word FROM {table_start}  WHERE idprime = {item}";
                //читаем ID нужных слов и записывем их в таблицу базы данных и все
                //rowString = reader.GetString("word");
                cmd.CommandText = sqlMax;
                reader.Close();
                reader = cmd.ExecuteReader();
                cmd.Parameters.Clear();

                if (reader.Read())
                {
                    // Console.Write($"idprime =  {reader.GetInt32(0)} ");
                    Console.Write($"idprime =  {reader.GetInt32("idprime")} ");
                    uint idprime = (uint)reader.GetInt32("idprime");
                    uint id = (uint)reader.GetInt32("id");
                    string str = reader.GetString("word");
                    reader.Close();

                    //пишем в таблицу
                    string sql = $"insert {tableToWrite} (idprime, id, word) VALUES ('{idprime}','{id}','{str}')";
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();

                }

            }


        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        finally
        {
            cmd.Parameters.Clear();
        }


    }

    /// <summary>
    /// записывает отдельные слова из служебных таблиц в другие
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="longWord"></param>
    /// <param name="table_start"></param>
    /// <param name="tableToWrite"></param>
    public void WriteDataForAnyTable(MySqlDataReader reader, List<uint> listAnalizData, string table_start, string columnID, string columnForRead, string tableToWrite, string columnToWrite)

    {
        try
        {


            foreach (var item in listAnalizData)
            {
                //читаем сохраненные IDPRIME
                string sqlMax = $"SELECT idprime, id, word FROM {table_start}  WHERE {columnID} = {item}";
                //читаем ID нужных слов и записывем их в таблицу базы данных и все
                //rowString = reader.GetString("word");
                cmd.CommandText = sqlMax;
                reader.Close();
                reader = cmd.ExecuteReader();
                cmd.Parameters.Clear();



                if (reader.Read())
                {
                    // Console.Write($"idprime =  {reader.GetInt32(0)} ");
                    Console.Write($"columnForRead =  {reader.GetInt32(columnID)} ");
                    //uint idprime = (uint)reader.GetInt32("idprime");
                    // uint id = (uint)reader.GetInt32("id");
                    string str = reader.GetString(columnForRead);
                    reader.Close();

                    //пишем в таблицу
                    string sql = $"insert {tableToWrite} ({columnToWrite}) VALUES ('{str}')";
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();

                }

            }


        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        finally
        {
            cmd.Parameters.Clear();
        }


    }

    /// <summary>
    /// Метод чтения заданного количества строк из заданных таблицы и столбца
    /// </summary>
    /// <param name="reader"> Поток для чтения</param>
    /// <param name="read_table"> Имя таблицы</param>
    /// <param name="read_column"> Имя столбца</param>
    /// <param name="number_of_linese"> Количество строк, которые необходимо прочитать</param>
    /// <param name="offset"> Количество пропускаемых строк с начала</param>
    /// <returns><List<string> - список строк/returns>
    public List<string> ReadDataTableString(MySqlDataReader reader, string read_table, string read_column, int number_of_linese = -1, int offset = 0)

    {
        List<string> word = new List<string>();
        //    struct rows()
        //   {
        //    public uint idprime;
        //public uint id;
        //public string = "";
        //}
        reader.Close();
        try
        {
            //получаем  количество данных записанных в таблицу всего
            string sqlMax = $"SELECT count(*) FROM {read_table}";
            cmd.CommandText = sqlMax;
            reader = cmd.ExecuteReader();
            reader.Read();
            int numberRow = reader.GetInt32(0);

            //считаем можно ли считать необходимое число строк
            if (numberRow == 0)
            {
                //в таблице нет записей
                //возвращаем пустой список
                Console.WriteLine($"Пустая таблица  {read_table}");
                return word;
            }

            if (number_of_linese == -1)
            {
                number_of_linese = numberRow;
            }
            if (offset > numberRow)
            {
                offset = 0;
                Console.WriteLine($"Слишком большой отступ. В {read_table} нет столько строк. Будет взят отступ = 0");

            }

            //хотим считать больше чем есть строк в таблице
            //считываем сколько есть
            if (numberRow < number_of_linese)
            {
                number_of_linese = numberRow;
            }

            if (number_of_linese > (numberRow - offset))
            {
                number_of_linese = numberRow - offset;
            }

            reader.Close();

            //читаем данные из таблицы
            string sql = $"SELECT {read_column} FROM {read_table}  LIMIT {number_of_linese} OFFSET {offset}";

            cmd.CommandText = sql;

            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                word.Add(reader.GetString(0));
            }

        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
            Console.WriteLine($"Ошибка чтения из таблицы {read_table}.");
        }
        finally
        {
            cmd.Parameters.Clear();
            reader.Close();
        }

        Console.WriteLine($"Данные из  таблицы {read_table} успешно считаны.");
        return word;
    }


}

