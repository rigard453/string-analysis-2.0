﻿using System.Text.RegularExpressions;

namespace СleaningLine;
/// <summary>
/// методы обработки строк
/// очистка от мусора и т.д.
/// методы предназначены для первичного накопления информации
/// </summary>
public class CleanerLine
{
    //URL для текущей страницы обработки
    private string actualUrl;

    /// <summary>
    /// HTML теги для удаления
    /// </summary>
    string[] cleanTeg = { "&nbsp;", "</strong>", "</p>", "<p >", "<p>", "</a>", "<head >", "<head>", "<link re", "<div", "<path", "</div>", "<ul>", "<li>", "</li>", "</ul>", "class=", "<a href=", "><", "<p style", "<span", "<img", "</span>", "<p " };

    /// <summary>
    /// символы русского языка для анализа
    /// </summary>
    char[] ruChar = { 'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё', 'Ж', 'ж', 'З', 'з',
                          'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
                          'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ',
                          'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь', 'Э', 'э', 'Ю', 'ю', 'Я', 'я'};

    char[] enChar = {'A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i',
                         'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r',
                         'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z'};
    char[] numbers = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

    /// <summary>
    /// символы пунктуации, которые используются для анализа
    /// </summary>
    char[] punctuationMarks = { ' ', '?', '.', '!', ':', ';', '-', ',' };

    /// <summary>
    /// символы пунктуации, которые используются для разбивки предложений
    /// </summary>
    char[] separatorSighn = { '?', '.', '!' };


    /// <summary>
    /// паттерны для анализа
    /// </summary>
    string[] patterns = {       @"[A-Z][a-z]+",
                                @"[A-Z][a-z]+\s+[A-Z][a-z]+",
                                @"[A-Z][a-z]+\s+[A-Z][a-z]+\s+[A-Z][a-z]+",
                                @"[A-Z][a-z]+\s+[A-Z][a-z]+\s+A-Z][a-z]+\s+[A-Z][a-z]+",
                                @"[A-Z][a-z]+\s+[a-z]+\s+[A-Z][a-z]+",
                                @"[А-Я][а-я]+",
                                @"[А-Я][а-я]+\s+[а-я]+\s+[А-Я][а-я]+",
                                @"[А-Я][а-я]+\s+[А-Я][а-я]+",
                                @"[А-Я][а-я]+\s+[А-Я][а-я]+\s+[А-Я][а-я]+",
                                @"[А-Я][а-я]+\s+[А-Я][а-я]+\s+[А-Я][а-я]+\s+[А-Я][а-я]+"};

    //максимальная длина слова 40 символов

    private int wordLength = 40;

    //public StringHandler( )
    //{
    //    actualUrl = "";
    //}
    /// <summary>
    /// установка URL для текущей страницы обработки
    /// </summary>
    /// <param name="URL"></param>
    /// <returns></returns>
    public bool setUrl(string URL)
    {
        if (URL == null)
        {
            Console.WriteLine($"");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"URL не определен!");
            Console.ResetColor();
            return false;
        }
        else
        {
            actualUrl = URL;
            return true;
        }


    }

    /// <summary>
    /// примитивная чистка от HTML разметки
    /// </summary>
    /// <param name="myString"></param>
    /// <returns></returns>
    public string cleanHTML(string myString)
    {

        foreach (var item in cleanTeg)
        {
            myString = myString.Replace(item, " ");
        }

        //string html = "<html><head><title>Home Page</title></head><body>Welcome</body></html>";
        //html = Regex.Replace(stringsPages[0], "<[^>]+>", string.Empty);
        //myString = myString.Replace("&nbsp;", " ");
        //myString = myString.Replace("</strong>", " ");
        //myString = myString.Replace("</p>", " ");
        //myString = myString.Replace("<p >", " ");
        //myString = myString.Replace("<p>", " ");
        //myString = myString.Replace("</a>", " ");
        return myString;
    }

    /// <summary>
    /// метод возвращает строку выбранную из исходной, куда включены только буквы русского и английского языка и цифры
    /// </summary>
    /// <param name="stringForAnaliz"> строка для обработки</param>
    /// <returns></returns>
    public string outOfRusionSighn(string stringForAnaliz)
    {
        //Нужно считать все символы русского языка в строк
        //вспомогательная строка
        string booferString = "";
        int index = 0;

        for (int i = 0; i < stringForAnaliz.Length; i++)
        {
            //русская часть
            bool t = ruChar.Contains(stringForAnaliz[i]);


            switch (t)
            {
                case true:
                    //проверка на пробел после русского символа
                    if (i + 1 < stringForAnaliz.Length)
                    {
                        //знаки пунктуации и разделители
                        if (punctuationMarks.Contains(stringForAnaliz[i + 1]))
                        {
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i]));
                            index++;
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i + 1]));
                            index++;
                        }
                        else
                        {
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i]));
                            index++;
                        }

                    }
                    continue;

                default:
                    break;
            }

            //английская часть
            bool p = enChar.Contains(stringForAnaliz[i]);


            switch (p)
            {
                case true:
                    //проверка на пробел после русского символа
                    if (i + 1 < stringForAnaliz.Length)
                    {
                        //знаки пунктуации и разделители
                        if (punctuationMarks.Contains(stringForAnaliz[i + 1]))
                        {
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i]));
                            index++;
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i + 1]));
                            index++;
                        }
                        else
                        {
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i]));
                            index++;
                        }

                    }

                    continue;

                default:
                    break;
            }

            //цифры
            bool n = numbers.Contains(stringForAnaliz[i]);


            switch (n)
            {
                case true:
                    //проверка на пробел после  символа
                    if (i + 1 < stringForAnaliz.Length)
                    {
                        //знаки пунктуации и разделители
                        if (punctuationMarks.Contains(stringForAnaliz[i + 1]))
                        {
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i]));
                            index++;
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i + 1]));
                            index++;
                        }
                        else
                        {
                            booferString = booferString.Insert(index, Convert.ToString(stringForAnaliz[i]));
                            index++;
                        }

                    }

                    break;
                default:
                    break;
            }

        }

        stringForAnaliz = booferString;

        return stringForAnaliz;
    }

    /// <summary>
    /// метод разбивает строки на предложения используя знаки припинания для разбивки
    /// </summary>
    /// <param name="stringForAnaliz"></param>
    /// <returns></returns>
    public string[] cutSentences(string stringForAnaliz)
    {
        //массив предложений
        string[] sentencesArray;
        sentencesArray = stringForAnaliz.Split(separatorSighn);

        List<string> bufer = new List<string>();

        //удалим предложения в которых нет пробелов
        for (int i = 0; i < sentencesArray.Length; i++)
        {
            if (sentencesArray[i].Contains(' '))
            {
                bufer.Add(sentencesArray[i]);
            }
        }

        sentencesArray = bufer.ToArray();
        return sentencesArray;
    }

    /// <summary>
    /// метод разбивает предложения, которые записаны в виде членов массива на слова и передает все это в виде зубчатого массива
    /// </summary>
    /// <param name="stringForAnaliz"></param>
    /// <returns></returns>
    public string[][] cutSentencesForWord(string[] sentencesArray)
    {
        //массив слов из предложений
        string[][] tempWordArray = new string[sentencesArray.Length][];

        int index = 0;



        foreach (var item in sentencesArray)
        {

            tempWordArray[index] = item.Split(' ');
            index++;
        }

        return tempWordArray;
    }

    /// <summary>
    /// метод поиска имен, названий, просто слов, которые начинаются с больших
    /// букв на русском и английском языках.
    /// При этом, ищутся как одиночные слова, так и их сочетания по 3.
    /// На английском сочетание больших и малых букв.
    /// </summary>
    /// <param name="sentencesArray"></param>
    /// <returns></returns>
    public string[][] findPatterns(string[] sentencesArray)
    {
        //создаем зубчатый массив для хранения слов найденных по паттернам
        string[][] wordsINSentense = new string[sentencesArray.Length][];

        //переменная для сохранения результатов каждый раз в новой строке зубчатого массива
        int rowSentens = 0;
        //идем по всем предложениям в массиве предложений
        foreach (var sentence in sentencesArray)
        {
            //проверяем предложение на все паттерны
            int index = 0;
            foreach (var pattern in patterns)
            {
                var words = Regex.Matches(sentence, pattern).Cast<Match>().Select(m => m.Value);
                int d;
                d = words.Count();
                if (d > 0)
                {
                    //myWords.Add(String.Join("", words));

                    if (wordsINSentense[rowSentens] == null)
                    {
                        wordsINSentense[rowSentens] = new string[d];
                    }
                    else
                    {
                        //измеряем длину массива
                        int lehgth = wordsINSentense[rowSentens].Length + d;
                        //создаем временный массив для хранения ранее обнаруженных строк
                        string[] tempstring = wordsINSentense[rowSentens];
                        //переназначаем новый массив для записи выделенных в предложении слов
                        wordsINSentense[rowSentens] = new string[lehgth];

                        //копируем старые слова из временного массива в новый увеличенный массив
                        Array.Copy(tempstring, wordsINSentense[rowSentens], tempstring.Length);

                        //wordsINSentense[0].CopyTo(tempstring, 0);
                    }


                    //Console.WriteLine("words.ElementAt(0) = " + words.ElementAt(0));
                    for (int i = 0; i < d; i++)
                    {
                        //    wordsOfSentense[0][index] = words.ElementAt(i);
                        wordsINSentense[rowSentens][index] = words.ElementAt(i);
                        index++;
                    }

                    //string[] substrings = words.Cop;
                }
                // Console.WriteLine($"words.Count() = {d}");

                // Console.WriteLine("index" + index);


            }
            //для зубчатого массива
            rowSentens++;
        }

        return wordsINSentense;
    }

    /// <summary>
    /// после поиска слов по паттернам, из полученных слов и словосочетаний
    /// удаляются слова, которые определены как одиночное, но входят в
    /// пары или тройки слов
    /// </summary>
    public string[][] deleteSingleWord(string[][] wordsINSentense)
    {
        //убираем повторы слов одинарной длины
        //убираем их если они встречаются в блоках слов с 2 и 3 словами
        //мерять можно по пробелам
        int wis = 0;
        foreach (var arr in wordsINSentense)
        {
            if (arr == null)
            {
                wis++;
                continue;
            }
            if (arr.Length <= 2)
            {
                wordsINSentense[wis] = null;
                wis++;
                //// wordsINSentense
                //for (int i = 0; i < arr.Length; i++)
                //{
                //    arr[i] = "";
                //}
                continue;
            }
            int arrwis = 0;
            //идем по словам в одном предложении
            foreach (var value in arr)
            {

                //здесь обрабатываем только одиночные слова
                if (value.Contains(' ') == false)
                {
                    //удаляем слишком длинные слова на РУССКОМ и АНГЛИЙСКОМ
                    //ограничимся 40 символами
                    if (value.Length > wordLength)
                    {
                        wordsINSentense[wis][arrwis] = "";
                        continue;
                    }
                    for (int i = 0; i < arr.Length; i++)
                    {
                        //одно слово
                        if (arr[i].Contains(' ') == false)
                        {
                            //если слово содержится в другой ячейке
                            if (arr[i].Contains(value))
                            {
                                // не сравнивать слова с самими собой
                                if (arrwis != i)
                                {
                                    wordsINSentense[wis][arrwis] = "";
                                    //прерываем цикл
                                    break;
                                }

                            }
                        }
                        //проверка на взождение слова в двойное или тройное слово
                        if (arr[i].Contains(' ') == true)
                        {
                            //если слово содержится в другой ячейке
                            if (arr[i].Contains(value))
                            {
                                // не сравнивать слова с самими собой
                                if (arrwis != i)
                                {
                                    wordsINSentense[wis][arrwis] = "";
                                    //прерываем цикл
                                    break;
                                }

                            }
                        }
                    }

                }
                //работаем с ДВОЙНЫМИ ПОВТОРАМИ
                if (value.Contains(' ') == true)
                {

                    // проверка на попадание слова в двойное слово
                    for (int i = 0; i < arr.Length; i++)
                    {
                        //одно слово
                        if (arr[i].Contains(' '))
                        {
                            if (arr[i].Contains(value) && i != arrwis)
                            {
                                wordsINSentense[wis][arrwis] = "";
                                //прерываем цикл
                                break;
                            }
                        }
                    }

                }


                arrwis++;
            }

            wis++;
        }

        //возвращаем значение
        return wordsINSentense;
    }

}

