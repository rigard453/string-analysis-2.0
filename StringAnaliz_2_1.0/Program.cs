﻿using MySQLDB;
using СleaningLine;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
// Программа обработки текстовой страницы, которую скачали из интернета
// Чистит теги разметки
// выделяет предложения
// Выделяет  слова из 2, 3, 4 слов, которые начинаются с большой буквы
// Пишет все это в массив, который нужно переносить в БД для дальнейшей обработки

// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

WorkSQLDB mySQLDB = new WorkSQLDB();

mySQLDB.MySQLDB();
// подключаемся к БД
string tablStart = "page_text";

mySQLDB.DBConnect();
MySqlConnection sqlConnection = mySQLDB.GetMySqlConnection();
MySqlCommand sqlCommand = mySQLDB.GetMySqlCommand();

//создаем поток для чтения

MySqlDataReader dataReader = mySQLDB.readTableDB(tablStart);

//класс для чистки текста
CleanerLine cleanerLine = new CleanerLine();

//читаем данные - страницу текста - и обрабатываем ее
//данные считываются в список
List<string> stringsPages = new List<string>();
stringsPages = mySQLDB.ReadDataTableString(dataReader, "page_text", "text", 1);
Console.WriteLine($"Полученная строка {stringsPages[0]}");
Console.WriteLine($"Длина полученной строки = {stringsPages[0].Length}");


// начинаем работать с текстом


// чистим от HTML разметки
stringsPages[0] = cleanerLine.cleanHTML(stringsPages[0]);

// Console.WriteLine("Получили строку : " + stringForAnaliz);
Console.WriteLine();
Console.WriteLine($"длина полученной после очистки строки = {stringsPages[0].Length} символов.");
Console.WriteLine();

// Оставляем только русские символы

stringsPages[0] = cleanerLine.outOfRusionSighn(stringsPages[0]);


//выделения предложения с большой буквы до ТОЧКИ или другого знака конца предложения
//предложения наиболее целостный структурный элемент текста
// исходя из этого и будем его выделять и с ним работать
//массив предложений
string[]? sentencesArray = null;

if (sentencesArray == null)
{
    sentencesArray = cleanerLine.cutSentences(stringsPages[0]);
}
else
{
    //клеим все в один массив

    sentencesArray = sentencesArray.Concat(cleanerLine.cutSentences(stringsPages[0])).ToArray();

}


//выделение слов в предложении и уборка  их повторов
//зубчатый массив слов в предложении и словосочетаний
string[][]? wordsINSentense = null;

if (wordsINSentense == null)
{
    wordsINSentense = cleanerLine.findPatterns(sentencesArray);
}
else
{
    wordsINSentense = wordsINSentense.Concat(cleanerLine.findPatterns(sentencesArray)).ToArray();
}

Console.WriteLine($"");
Console.WriteLine($"Результаты чистки повторов слов в предложении");

//чистим от одиночных слов
wordsINSentense = cleanerLine.deleteSingleWord(wordsINSentense);
//myHelp.printArray(wordsINSentense);


Console.WriteLine();
Console.WriteLine($"длина полученной строки после очистки = {stringsPages[0].Length} символов.");
//Console.WriteLine($"получили = {stringForAnaliz}.");



Console.WriteLine($"Finish.");